SAMTools BAM to FASTQ GeneFlow App
==================================

Version: 1.10-01

This GeneFlow app wraps the samtools bam2fq functionality, and also allows filtering by SAM flags.  

Inputs
------
1. input: SAM or BAM file.  

Parameters
----------
1. include_reads_all: Include reads with all flag values present, where flag is an integer. Default 0.
2. include_reads_none: Include reads with none of the flag values present, where flag is an integer. Default 0.
3. exlude_reads_all: Exclude reads with all flag values present, where flag is an integer. Default 0.
4. output: Output FASTQ file.
